<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Models;

use Inc\Base\Config;

class ContactFormSettings extends BaseModel
{
	public $to;
	public $from;
	public $subject;
	public $replay;
	public $message;

	public function rules()
	{
		return [
			[['to', 'from', 'subject', 'replay', 'message'], 'required'],
			[['message'], ['max' => 10000]],
			[['from', 'subject'], ['max' => 255]],
			[['subject', 'from'], ['min' => 2] ],
			[['to'], 'email'],
		];
	}

	public static function getSettings()
	{
		$settingsArr = array(
			'to' => [],
			'from' => [],
			'subject' => [],
			'message' => [],
			'replay' => [],
			'short_code' => Config::$plugin_short_code,
		);

		if ( $settings = get_option( 'contact_form_settings') ) {
			$settings = unserialize($settings);
			if ( ! empty($settings) ) {
				$settingsArr['to'] = isset ($settings['to']) ? $settings['to'] : '';
				$settingsArr['from'] = isset($settings['from']) ? $settings['from'] : '';
				$settingsArr['subject'] = isset($settings['subject']) ? $settings['subject'] : '';
				$settingsArr['message'] = isset($settings['message']) ? $settings['message'] : '';
				$settingsArr['replay'] = isset($settings['replay']) ? $settings['replay'] : '';
			}
		}
		return $settingsArr;
	}

	public static function install_default_settings()
	{
		if ( !get_option( 'contact_form_settings') ) {
			$content = serialize([
				'to' => get_option('admin_email'),
				'from' => sprintf( '%s <%s>',
					get_bloginfo( 'name' ), self::from_email() ),
				'subject' => sprintf( '%1$s %2$s',
					get_bloginfo( 'name' ), '[your-subject]' ),
				'message' =>
					'Message Body:'
					. "\n" . '[your-message]' . "\n\n"
					. '-- ' . "\n"
					. sprintf( 'This e-mail was sent from a contact form on %1$s (%2$s)',
						get_bloginfo( 'name' ), get_bloginfo( 'url' ) ),
				'replay' => 'Reply-To: [your-name] <[your-email]>',
			]);
			return add_option('contact_form_settings', $content, '');
		}
		return;
	}

	protected static function from_email() {
		$admin_email = get_option( 'admin_email' );
		$sitename = strtolower( $_SERVER['SERVER_NAME'] );

		if ( substr( $sitename, 0, 4 ) == 'www.' ) {
			$sitename = substr( $sitename, 4 );
		}

		if ( strpbrk( $admin_email, '@' ) == '@' . $sitename ) {
			return $admin_email;
		}

		return 'wordpress@' . $sitename;
	}


	public function save($validate = true)
	{
		if ($validate) {
			if ( !$this->validate()) {
				return false;
			}
		}

		$content = serialize([
			'to' => $this->to,
			'from' => $this->from,
			'subject' => $this->subject,
			'message' => $this->message,
			'replay' => $this->replay
		]);

		if ( get_option( 'contact_form_settings') ) {
			return update_option( 'contact_form_settings', $content );

		} else {
			return add_option('contact_form_settings', $content, '');
		}

		return false;
	}

}
