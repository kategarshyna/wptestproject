<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Models;

class ContactForm extends BaseModel
{
	public $name;
	public $email;
	public $subject;
	public $message;

	const post_type = 'pl_contact_form';
	const post_title = 'Contact';

	public function rules()
	{
		return [
			[['name', 'email', 'subject', 'message'], 'required'],
			[['message'], ['max' => 10000]],
			[['subject', 'name'], ['min' => 2] ],
			[['email'], 'email'],
			[['name', 'subject'], ['pattern' => '/^[\p{L}\s\']+$/uis']],
		];
	}

	public function contact($email)
	{
		if ($this->validate()) {

			$settings = ContactFormSettings::getSettings();
			$email = isset($settings['to']) ?  $settings['to'] : $email;
			$subject = isset($settings['subject']) ?
				str_replace('[your-subject]', $this->subject, $settings['subject'])
				: $this->subject;
			$message = isset($settings['message']) ?
				str_replace('[your-message]', $this->message, $settings['message'])
				: $this->message;
			$replay = isset($settings['replay']) ?
				str_replace('[your-name]', $this->name, $settings['replay'])
				: $this->name;
			$replay = isset($settings['replay']) ?
				str_replace('[your-email]', $this->email, $replay)
				: $this->email;

			if (wp_mail( $email, $subject, $message, [$replay])) {
				return $this->saveContact();
			}
		}

		return false;
	}

	public function register_post_type() {
		return register_post_type( self::post_type, array(
			'labels' => array(
				'name' => __( 'Contact Form Contacts', self::post_type ),
				'singular_name' => __( 'Contact Form Contact', self::post_type ),
			),
			'rewrite' => false,
			'query_var' => false,
		) );
	}

	public function saveContact()
	{
		if ( !post_type_exists( self::post_type ) ) {
			$this->register_post_type();
		}

		if ( post_type_exists( self::post_type ) ) {
			$post_content = serialize([
				'subject' => $this->subject,
				'message' => $this->message,
				'replay' => [
					"Reply-To: $this->name <$this->email>"
				]
			]);
			if ( wp_insert_post( array(
				'post_type' => self::post_type,
				'post_status' => 'publish',
				'post_title' => self::post_title,
				'post_content' => wp_slash(trim( $post_content )),
			), true )) {
				return true;
			}
		}
		return false;
	}

	public static function getContacts($id = null)
	{
		if ( !is_null($id) ) {
			return get_post( $id );
		}
		return get_posts( array( 'post_type' => self::post_type, 'numberposts' => -1 ) );
	}
}
