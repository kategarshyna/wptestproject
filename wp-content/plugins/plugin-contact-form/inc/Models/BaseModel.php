<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Models;

class BaseModel
{
	public $field = false;

	public $errors = [];

	public function getErrors() {
		return $this->errors;
	}

	public function rules()
	{
		return [];
	}


	public function load($data)
	{
		$this->setAttributes($data);
		return true;
	}

	public function setAttributes($values)
	{
		if (is_array($values)) {
			foreach ($values as $name => $value) {
				if (property_exists($this, $name)) {
					$this->$name = $value;
				}
			}
		}
	}
	public function validationRule($rule, $fieldName, $field, $value = null)
	{
		switch ($rule) {
			case 'email' :
				return preg_match('/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $field) ?
						[] : [$fieldName => ucfirst($fieldName) . ' is not valid'];
				break;
			case 'max' :
				return mb_strlen($field) <= $value ? [] : [$fieldName => ucfirst($fieldName) . " must contain not more then $value characters."];
				break;
			case 'min' :
				return mb_strlen($field) >= $value ? [] : [$fieldName => ucfirst($fieldName) . " must contain not less then $value characters."];
				break;
			case 'required' :
				return empty(trim($field)) ? [$fieldName => ucfirst($fieldName) . " can not be blank."] : [];
				break;
		}
	}

	public function validate()
	{
		$attribute = $this->field;
		if ($attribute) {
			$validateResult = false;
			if (property_exists($this, $attribute)) {
				foreach ($this->rules() as $item) {
					list($keys, $rule) = $item;
					if (in_array($attribute, (array)$keys)) {
						$ruleName = is_array($rule) ? key($rule) : $rule;
						$ruleValue = is_array($rule) ? current($rule) : null;
						$validateFieldResult = $this->validationRule($ruleName, $attribute, $this->{$attribute}, $ruleValue);
						if (!empty($validateFieldResult)) {
							$this->errors = array_merge( $this->errors, $validateFieldResult);
							return $validateResult;
						} else {
							$this->errors = array_merge( $this->errors, [$attribute => 'success']);
						}
					}
				}
			}
			return $validateResult;
		} else {
			$validateResult = true;
			$this->errors = [];
			foreach ($this->rules() as $item) {
				list($keys, $rule) = $item;
				$ruleName = is_array($rule) ? key($rule) : $rule;
				$ruleValue = is_array($rule) ? current($rule) : null;
				foreach ($keys as $key) {
					if (property_exists($this, $key) && !key_exists($key, $this->errors)) {
						$validateFieldResult = $this->validationRule($ruleName, $key, $this->$key, $ruleValue);
						if (!empty($validateFieldResult)) {
							$this->errors = array_merge( $this->errors, $validateFieldResult);
							$validateResult = false;
						}
					}
				}
			}
			return $validateResult;
		}
	}
}
