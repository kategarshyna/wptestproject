<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Api;

class SettingsApi
{
	public $admin_pages = array();

	public function register()
	{
		if ( !empty($this->admin_pages) ) {
			add_action( 'admin_menu', array($this, 'addAdminMenu'));
		}
	}

	public function addPages (array $pages)
	{
		$this->admin_pages = $pages;
		return $this;
	}

	public function addAdminMenu()
	{
		$main_page = true;
		$parent_slug = '';
		foreach ($this->admin_pages as $page) {
			if ($main_page) {
				add_menu_page(
					$page['page_title'],
					$page['menu_title'],
					$page['capability'],
					$page['menu_slug'],
					$page['callback'],
					$page['icon_url'],
					$page['position']
				);
				$parent_slug = $page['menu_slug'];
				$main_page = false;
			}

			add_submenu_page(
				$parent_slug,
				$page['page_title'],
				$page['menu_title'],
				$page['capability'],
				$page['menu_slug'],
				$page['callback']
			);
		}
	}
}
