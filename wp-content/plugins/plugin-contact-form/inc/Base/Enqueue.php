<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Base;

class Enqueue
{
	public function register ()
	{
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	function enqueue()
	{
		wp_enqueue_style( 'mypluginstyle', Config::$plugin_dir_url . 'assets/style.css');

		wp_register_script(
			'mypluginjs',
			Config::$plugin_dir_url . 'assets/script.js',
			array( 'jquery' )
		);
		wp_enqueue_script( 'mypluginjs' );

		wp_localize_script( 'mypluginjs', 'ajax_object',
			array(
				'ajax_url' => admin_url( 'admin-ajax.php' )
			)
		);
	}
}
