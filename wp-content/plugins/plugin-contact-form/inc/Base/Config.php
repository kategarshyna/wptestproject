<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Base;

class Config
{
	public static $plugin_dir_path;
	public static $plugin_dir_url;
	public static $plugin;
	public static $plugin_settings_page;
	public static $plugin_contacts_page;
	public static $plugin_short_code;

	public function register()
	{
		self::$plugin_dir_path = plugin_dir_path( dirname( __FILE__, 2) );
		self::$plugin_dir_url = plugin_dir_url( dirname( __FILE__, 2 ));
		self::$plugin = plugin_basename( dirname( __FILE__, 3 ) ) . 'plugin-contact-form.php';
		self::$plugin_settings_page = 'contact_form';
		self::$plugin_contacts_page = 'contacts';
		self::$plugin_short_code = 'plugin-contact-form';
	}
}
