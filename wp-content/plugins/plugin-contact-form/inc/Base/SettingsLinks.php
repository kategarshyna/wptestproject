<?php
/**
 * @package ContactFormPlugin
 */
namespace  Inc\Base;

class SettingsLinks
{

	public function register()
	{
		add_filter( "plugin_action_links_" . Config::$plugin, array( $this, 'settings_link' ) );
	}

	public function settings_link ( $links )
	{
		$settings_link = '<a href="admin.php?page=' . Config::$plugin_settings_page . '">Settings</a>';
		array_push($links, $settings_link);
		return $links;
	}
}
