<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\Config;
use Inc\Models\ContactForm;
use Inc\Models\ContactFormSettings;

class Admin
{
	public $settings;

	public function __construct()
	{
		$this->settings = new SettingsApi();
	}

	public function register ()
	{
		$pages = [
			[
				'page_title' => 'Contact Form',
				'menu_title' => 'ContactForm',
				'capability' => 'manage_options',
				'menu_slug' => 'contact_form',
				'callback' => [$this, 'admin_form_editor'],
				'icon_url' => 'dashicons-editor-table',
				'position' => 110
			],
			[
				'page_title' => 'Contacts',
				'menu_title' => 'Contacts',
				'capability' => 'manage_options',
				'menu_slug' => 'contacts',
				'callback' => [$this, 'admin_all_contacts'],
			]
		];
		$this->settings->addPages( $pages )->register();
		ContactFormSettings::install_default_settings();
		add_action( 'wp_ajax_save_contact_form_settings', array($this, 'save_contact_form_settings') );
	}


	public function admin_form_editor()
	{
		$mail = ContactFormSettings::getSettings();
		require_once Config::$plugin_dir_path . "/themeplates/admin/form_editor.php";
	}

	public function admin_all_contacts()
	{
		if ( isset($_GET['contact_id'] ) ) {
			$contact = ContactForm::getContacts( $_GET['contact_id'] );
			require_once Config::$plugin_dir_path . "/themeplates/admin/contact.php";
			die;
		}
		$contacts = ContactForm::getContacts();
		require_once Config::$plugin_dir_path . "/themeplates/admin/contacts.php";
	}

	public function save_contact_form_settings()
	{
		$contact_form_settings = new ContactFormSettings();
		if ($contact_form_settings->load($_POST) && $contact_form_settings->save()){
			return wp_send_json([
				'status' => true,
				'messages' => 'Saved successfully.'
			]);
			wp_die();
		}

		return wp_send_json([
			'status' => false,
			'messages' => $contact_form_settings->getErrors()
		]);

		wp_die();
	}

}
