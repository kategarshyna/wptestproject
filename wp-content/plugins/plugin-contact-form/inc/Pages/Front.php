<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Pages;

use Inc\Base\Config;
use Inc\Models\ContactForm;

class Front
{
	public function register()
	{
		add_action( 'wp_ajax_front_contact_form_action', array($this, 'front_contact_form_action') );
		add_action( 'wp_ajax_nopriv_front_contact_form_action', array($this, 'front_contact_form_action') );
		add_shortcode( Config::$plugin_short_code, array( $this, 'run' ) );
	}

	public function run()
	{
		require_once Config::$plugin_dir_path . "/themeplates/front.php";
		return;
	}

	public function front_contact_form_action()
	{
		$contact_form = new ContactForm();
		if ($contact_form->load($_POST) && $contact_form->contact(get_option('admin_email'))){
			return wp_send_json([
				'status' => true,
				'messages' => 'Thank you! Your message send successfull.'
			]);
			wp_die();
		}

		return wp_send_json([
			'status' => false,
			'messages' => $contact_form->getErrors()
		]);

		wp_die();
	}

}
