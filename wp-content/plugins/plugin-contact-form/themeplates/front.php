<div id="plugin_contact_form">
	<h3>Contact Us</h3>
	<form>
		<div class="form-group">
			<input name="name" placeholder="Enter your name.." value="" type="text"/>
			<div class="error-msg"></div>
		</div>
		<div class="form-group">
			<input name="email" placeholder="Enter your email.." value="" type="email" class="form-control"/>
			<div class="error-msg"></div>
		</div>
		<div class="form-group">
			<input name="subject" placeholder="Enter your subject.." value="" type="text" class="form-control"/>
			<div class="error-msg"></div>
		</div>
		<div class="form-group">
			<textarea name="message" placeholder="Enter your message.." type="text" class="form-control"></textarea>
			<div class="error-msg"></div>
		</div>
		<div class="form-group">
			<button type="button" class="btn btn-default submit form-control">Send <img src="<?php echo \Inc\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" id="loader-contact-form-plugin" style="display:none"/ ></button>
		</div>
	</form>
	<p class="result-msg"></p>
</div>
