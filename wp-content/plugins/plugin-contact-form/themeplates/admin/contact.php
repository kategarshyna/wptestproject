<?php

use \Inc\Base\Config;

/**
 * @var ArrayObject $contact
 */
?>

<h1>Contact ID <?php echo $contact->ID ?></h1>
<a href="admin.php?page=<?php echo Config::$plugin_contacts_page ?>">Back</a>


<?php $params = unserialize($contact->post_content) ?>
<table class="form-table container">
	<tbody>
	<tr>
		<th scope="row">
			<label>Date</label>
		</th>
		<td>
			<label><?php echo $contact->post_date ?></label>
		</td>
	</tr>
	<tr>
		<th scope="row">
			<label>Subject</label>
		</th>
		<td>
			<label><?php echo esc_html( $params['subject'] ) ?></label>
		</td>
	</tr>

	<tr>
		<th scope="row">
			<label>Replay-To</label>
		</th>
		<td>
			<label><?php echo esc_html( reset ( $params['replay'] ) ) ?></label>
		</td>
	</tr>
	<tr>
		<th scope="row">
			<label>Message</label>
		</th>
		<td>
			<label><?php echo esc_html( $params['message'] ) ?></label>
		</td>
	</tr>
	</tbody>
</table>
