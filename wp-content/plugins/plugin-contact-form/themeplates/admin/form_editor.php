<div class="container">
	<h1>Edit Contact Form</h1>
	<p class="description">
	<label for="shortcode">Copy this shortcode and paste it into your post, page, or text widget content:</label>
	<span><input type="text" id="shortcode" onfocus="this.select();" readonly="readonly" class="large-text code" value="[<?php echo esc_attr( $mail['short_code'] ); ?>]" /></span>
	</p>

	<div id="plugin_settings_contact_form">
		<form>
			<table class="form-table">
				<tbody>
				<tr>
					<th scope="row">
						<label for="to">To</label>
					</th>
					<td>
						<input name="to" value="<?php echo esc_html( $mail['to'] ) ?>" type="email" class="large-text code" size="70"/>
						<div class="error-msg"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="from">From</label>
					</th>
					<td>
						<input name="from" value="<?php echo esc_html( $mail['from'] ) ?>" type="text" class="large-text code" size="70"/>
						<div class="error-msg"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="sabject">Subject</label>
					</th>
					<td>
						<input name="subject" value="<?php echo esc_html( $mail['subject'] ) ?>" type="text" class="large-text code" size="70"/>
						<div class="error-msg"></div>
					</td>
				</tr>

				<tr>
					<th scope="row">
						<label for="replay">Replay-To</label>
					</th>
					<td>
						<input name="replay" value="<?php echo esc_html( $mail['replay'] ) ?>" type="text" class="large-text code" size="70"/>
						<div class="error-msg"></div>
					</td>
				</tr>
				<tr>
					<th scope="row">
						<label for="Message">Message</label>
					</th>
					<td>
						<textarea rows="7" name="message" type="text" class="large-text code" size="70"><?php echo esc_html( $mail['message'] ) ?></textarea>
						<div class="error-msg"></div>
					</td>
				</tr>
				<tr>
					<td>
						<button type="button" class="button submit form-control">Save <img src="<?php echo \Inc\Base\Config::$plugin_dir_url . 'assets/loader.gif'?>" id="loader-contact-form-plugin" style="display:none"/ ></button>
					</td>
				</tr>
		</form>
		<p class="result-msg"></p>
	</div>
</div>
