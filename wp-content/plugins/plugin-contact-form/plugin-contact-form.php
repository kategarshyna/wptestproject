<?php
/*
 * @package ContactFormPlugin
 *
 * Plugin Name: Contact Form Plugin
 * Plugin URI: https://bitbucket.org/kategarshyna/wptestproject/src
 * Description: Plugin for adding a contact form to the site
 * Author: Kate Garshyna
 * Version: 1.0
 * Author URI: https://github.com/kategarshyna
 * License: GPLv2 or later
 * Text Domain: wptestproject.amtago.ml
*/

use Inc\Init;

defined( 'ABSPATH' ) or die;

if ( file_exists(dirname( __FILE__ ) . '/vendor/autoload.php') ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

if ( class_exists('Inc\\Init') ) {
	Init::register_services();
}
