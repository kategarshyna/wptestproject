<?php
/**
 * @package ContactFormPlugin
 */

namespace Inc\Pages;

use Inc\Api\SettingsApi;
use Inc\Base\BaseController;

class Admin extends BaseController
{
	public $settings;

	public function __construct()
	{
		$this->settings = new SettingsApi();
	}

//	protected $plugin_settings_page;

//	public function __construct()
//	{
//		$this->plugin_settings_page = PLUGIN_SETTINGS_PAGE;
//		$this->plugin_path = PLUGIN_PATH;
//	}

	public function register ()
	{
		$pages = [
			[
				'page_title' => 'Contact Form',
				'menu_title' => 'ContactForm',
				'capability' => 'manage_options',
				'menu_slug' => $this->plugin_settings_page,
				'callback' => function() {echo 'Lol';},
				'icon_url' => 'dashicons-store',
				'position' => 110
			],
			[
				'page_title' => 'New Contact Form',
				'menu_title' => 'Testl',
				'capability' => 'manage_options',
				'menu_slug' => 'test_plugin',
				'callback' => function() {echo 'ext';},
				'icon_url' => 'dashicons-external',
				'position' => 110
			]
		];
//		add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
		$this->settings->addPages( $pages )->register();
	}

//	public function add_admin_pages ()
//	{
//		add_menu_page ('Contact Form', 'ContactForm', 'manage_options', $this->plugin_settings_page,
//			[$this, 'admin_index'], 'dashicons-store', 110);
//	}
//
//	public function admin_index()
//	{
//		require_once "$this->plugin_path/themeplates/admin.php";
//	}
}
