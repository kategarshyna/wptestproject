<?php
/*
 * @package ContactFormPlugin
 *
 * Plugin Name: 2Contact Form Plugin
 * Plugin URI: https://bitbucket.org/kategarshyna/wptestproject/src
 * Description: Plugin for adding a contact form to the site
 * Author: Kate Garshyna
 * Version: 1.0
 * Author URI: https://github.com/kategarshyna
 * License: GPLv2 or later
 * Text Domain: wptestproject.amtago.ml
*/

defined( 'ABSPATH' ) or die;

if ( file_exists(dirname( __FILE__ ) . '/vendor/autoload.php') ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}

if ( class_exists('Inc\\Init') ) {
	\Inc\Init::register_services();
}

function activate_contact_form_plugin () {
	\Inc\Base\Activate::activate();
}
//activation
register_activation_hook( __FILE__, 'activate_contact_form_plugin' );



function deactivate_contact_form_plugin () {
	\Inc\Base\Deactivate::deactivate();
}

//deactivate
register_deactivation_hook( __FILE__, 'deactivate_contact_form_plugin' );
