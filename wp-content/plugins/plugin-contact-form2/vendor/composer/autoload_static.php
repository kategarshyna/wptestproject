<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcec21a68263dbf2ad5654255ea849ad8
{
    public static $prefixLengthsPsr4 = array (
        'I' => 
        array (
            'Inc\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Inc\\' => 
        array (
            0 => __DIR__ . '/../..' . '/inc',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcec21a68263dbf2ad5654255ea849ad8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcec21a68263dbf2ad5654255ea849ad8::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
